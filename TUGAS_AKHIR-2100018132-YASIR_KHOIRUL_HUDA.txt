include 'emu8086.inc'
org 100h
;
define_print_string
define_scan_num
define_print_num
define_print_num_uns 
;
.model small
.data  
    jumlah db 0dh,0ah,'jumlah : ' ,0
    program db 0dh,0ah,'program ' ,0
    blank db 0dh,0ah,'',0
    tampil db 0dh,0ah,'total harga : ' ,0
    hrg1 db 0dh,0ah,'masukkan harga barang 1 : ',0
    hrg2 db 0dh,0ah,'masukkan harga barang 2 :',0
    hrg3 db 0dh,0ah,'masukkan harga barang 3 :',0
    operand1 dw 0
    operand2 dw 0
    operand3 dw 0 
    hasil dw 0
    
.code
.startup

start_:

input proc
    ;;;;;;
    
    lea si,hrg1
    call print_string
    lea si,blank
    call print_string
    call scan_num
    mov operand1,cx
    ;;;;;;
    
    lea si,hrg2
    call print_string
    lea si,blank
    call print_string
    call scan_num
    mov operand2,cx
    ;;;;;;
    lea si,hrg3
    call print_string
    lea si,blank
    call print_string
    call scan_num
    mov operand3,cx
    
input endp

tampil_jumlah proc
    lea si,tampil
    call print_string
    mov ax,hasil
    mov ax,operand1
    mov bx,operand2
    mov cx,operand3
    add bx,cx
    add ax,bx
    mov hasil,ax
    call print_num
tampil_jumlah endp

